<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                        'name' => 'manager',
                        'email' => 'a@a.com',
                        'password' => Hash::make('12345678'),
                        'role'=>'manager',
                        'created_at' => date('Y-m-d G:i:s'),
                ],
                [
                    'name' => 'salesrep1',
                    'email' => 'b@b.com',
                    'password' => Hash::make('12345678'),
                    'role'=>'salesrep',
                    'created_at' => date('Y-m-d G:i:s'),
              ],
              [
                'name' => 'salesrep2',
                'email' => 'c@c.com',
                'password' => Hash::make('12345678'),
                'role'=>'salesrep',
                'created_at' => date('Y-m-d G:i:s'),
              ],
                    ]);
    }
}
