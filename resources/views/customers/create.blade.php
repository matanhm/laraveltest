
@extends('layouts.app')
@section('content')

<h1>Create New Customer</h1>
<form method = 'post' action="{{action('CustomerController@store')}}">
{{csrf_field()}}

<div class = "form-group">
    <label for = "name">Customer's Name</label>
    <input type= "text" class = "form-control" name= "name"><br>
    <label for = "email">Customer's Email</label>
    <input type= "text" class = "form-control" name= "email"><br>
    <label for = "phone">Customer's Phone</label>
    <input type= "text" class = "form-control" name= "phone"><br>
</div>

<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="add">
</div>

</form>

<a href="{{route('customers.index')}}">Back to customers list </a>
@endsection