
@extends('layouts.app')
@section('content')

<h1>This is the customers list</h1>

<a href="{{route('customers.create')}}">Create New Customer </a>
<ul>
    @foreach($customers as $customer)
    <li>
    
     Creator Id: {{$customer->user->name}}  Name: {{$customer->name}}  Email:{{$customer->email}}    Phone:{{$customer->phone}}    
      <a href= "{{route('customers.edit', $customer->id )}}"> Edit </a> @cannot('salesrep')<a href= "{{route('delete', $customer->id )}}"> @endcannot Delete Customer</a>
      @if($customer->status == 1)
       
       @else
       @cannot('salesrep') <a href= "{{route('statusupdate', $customer->id )}}">  Deal Closed</a>@endcannot
    </li>
    @endif
    @endforeach
</ul>


@endsection