
@extends('layouts.app')
@section('content')

<h1>Edit cutomer</h1>
<form method = 'post' action="{{action('CustomerController@update', $customer->id)}}">
@csrf
@method('PATCH')
<div class = "form-group">
    <label for = "name">Customer Name to Update:</label>
    <input type= "text" class = "form-control" name= "title" value = "{{$customer->name}}">
    <label for = "email">Customer email to Update:</label>
    <input type= "text" class = "form-control" name= "title" value = "{{$customer->email}}">
    <label for = "phone">Customer phone to Update:</label>
    <input type= "text" class = "form-control" name= "title" value = "{{$customer->phone}}">
</div>

<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Update">
</div>

</form>

<a href="{{route('customers.index')}}">Back to customers list </a>

@endsection