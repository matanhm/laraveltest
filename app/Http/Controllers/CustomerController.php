<?php

namespace App\Http\Controllers;
use App\Customer; 
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
       // $id=1; //assuming that user 1 is logged in
        $customers = Customer::all(); 
        
       return view('customers.index', ['customers'=>$customers]);
   

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('manager')||Gate::denies('salesrep')) {
            abort(403,"Sorry you are not allowed to create a new customer..");
        }
        return view ('customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer = new Customer();
        $id = 1;
        $customer->name = $request->name;
        $customer->email = $request->email;
        $customer->phone = $request->phone;
        $customer->user_id = $id;
        $customer->save();
        return redirect('customers');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        if (Gate::denies('manager','salesrep')) {
            abort(403,"Sorry you are not allowed to create a new customer..");
        } 
    
        $customer = Customer::find($id);
        return view('customers.edit', compact('customer'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customer = Customer::find($id);
        $customer -> update($request->all());
        return redirect('customers');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"Sorry you are not allowed to create a new customer..");
        } 
        $customer = Customer::find($id);
        $customer->delete();
        return redirect('customers');
    }

    public function update_status($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"You Cannot do that sorry...");
       }
        $customer = Customer::find($id);
        $customer->status = 1;
        $customer -> update();
        return redirect('customers');
    }
}
